# colorstatic
Random color animations by character frames for fun in the command line. The default, especially at a low font size looks like old television static. Many options and cool things you can do.
